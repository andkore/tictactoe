package com.andrewliebenow.tictactoe;

import java.util.ArrayList;
import java.util.List;

public class GameLogic {
	final static int boardSize = 3;

	static enum Player {
		X, O
	}

	static enum SpotOwner {
		X, O, FREE
	}

	static enum Outcome {
		IN_PROGRESS, DRAW, X_WINS, O_WINS
	}

	static class GameState {
		Outcome outcome;
		Player currentPlayer;
		GameBoard gameBoard;

		GameState() {
			this.outcome = Outcome.IN_PROGRESS;
			this.currentPlayer = Player.X;
			this.gameBoard = new GameBoard();
		}

		void processPlay(SpotLocation spotLocation) {
			if (!this.outcome.equals(Outcome.IN_PROGRESS)) {
				return;
			}

			Spot spot = this.gameBoard.getSpot(spotLocation);
			if (!spot.spotIsFree()) {
				return;
			}

			spot.takeSpot(this.currentPlayer);
			boolean victoryObtains = this.gameBoard.victoryObtains();
			if (victoryObtains) {
				if (this.currentPlayer.equals(Player.X)) {
					this.outcome = Outcome.X_WINS;
				} else if (this.currentPlayer.equals(Player.O)) {
					this.outcome = Outcome.O_WINS;
				}
			} else {
				boolean gameDrawn = !this.gameBoard.freeSpotExists();
				if (gameDrawn) {
					this.outcome = Outcome.DRAW;
				} else {
					this.cycleNextPlayer();
				}
			}
		}

		void cycleNextPlayer() {
			if (this.currentPlayer.equals(Player.X)) {
				this.currentPlayer = Player.O;
			} else if (this.currentPlayer.equals(Player.O)) {
				this.currentPlayer = Player.X;
			}
		}
	}

	static class Spot {
		int row;
		int column;
		SpotOwner spotOwner;

		Spot(int row, int column, SpotOwner spotOwner) {
			this.row = row;
			this.column = column;
			this.spotOwner = spotOwner;
		}

		boolean spotIsFree() {
			return this.spotOwner.equals(SpotOwner.FREE);
		}

		void takeSpot(Player player) {
			SpotOwner spotOwner = null;
			if (player.equals(Player.X)) {
				spotOwner = SpotOwner.X;
			} else if (player.equals(Player.O)) {
				spotOwner = SpotOwner.O;
			}
			this.spotOwner = spotOwner;
		}
	}

	static class SpotLocation {
		int row;
		int column;

		SpotLocation(int row, int column) {
			this.row = row;
			this.column = column;
		}
	}

	static class GameBoard {
		List<Spot> spots;

		public GameBoard() {
			this.spots = new ArrayList<Spot>();
			for (int i = 0; i < GameLogic.boardSize; ++i) {
				for (int j = 0; j < GameLogic.boardSize; ++j) {
					Spot spot = new Spot(i, j, SpotOwner.FREE);
					this.spots.add(spot);
				}
			}
		}

		Spot getSpot(SpotLocation spotLocation) {
			for (Spot spot : this.spots) {
				if (spot.row == spotLocation.row
						&& spot.column == spotLocation.column) {
					return spot;
				}
			}
			return null;
		}

		boolean spotIsFree(SpotLocation spotLocation) {
			return this.getSpot(spotLocation).spotIsFree();
		}

		boolean victoryObtains() {
			List<List<Spot>> listOfWinningSpots = splitGameBoardIntoWinningSpots();
			boolean victoryObtains = false;
			for (List<Spot> winningSpots : listOfWinningSpots) {
				boolean victoryFromWinningSpots = allSpotsOwnedByXOrY(winningSpots);
				if (victoryFromWinningSpots) {
					victoryObtains = true;
					break;
				}
			}

			return victoryObtains;
		}

		List<List<Spot>> splitGameBoardIntoWinningSpots() {
			List<List<Spot>> listOfWinningSpots = new ArrayList<List<Spot>>();

			for (int i = 0; i < GameLogic.boardSize; ++i) {
				List<Spot> column = new ArrayList<Spot>();
				for (Spot spot : this.spots) {
					if (spot.column == i) {
						column.add(spot);
					}
				}
				listOfWinningSpots.add(column);
			}

			for (int i = 0; i < GameLogic.boardSize; ++i) {
				List<Spot> row = new ArrayList<Spot>();
				for (Spot spot : this.spots) {
					if (spot.row == i) {
						row.add(spot);
					}
				}
				listOfWinningSpots.add(row);
			}

			List<Spot> leftDiagonal = new ArrayList<Spot>();
			for (int i = 0; i < GameLogic.boardSize; ++i) {
				for (Spot spot : this.spots) {
					if (spot.row == i && spot.column == i) {
						leftDiagonal.add(spot);
					}
				}
			}
			listOfWinningSpots.add(leftDiagonal);

			List<Spot> rightDiagonal = new ArrayList<Spot>();
			for (int i = 0; i < GameLogic.boardSize; ++i) {
				int rowIndex = i;
				int columnIndex = (GameLogic.boardSize - 1) - i;
				for (Spot spot : this.spots) {
					if (spot.row == rowIndex
							&& spot.column == columnIndex) {
						rightDiagonal.add(spot);
					}
				}
			}
			listOfWinningSpots.add(rightDiagonal);

			return listOfWinningSpots;
		}

		boolean freeSpotExists() {
			for (Spot spot : this.spots) {
				if (spot.spotIsFree()) {
					return true;
				}
			}

			return false;
		}
	}

	static boolean allSpotsOwnedByXOrY(List<Spot> spots) {
		boolean allSpotsAreX = true;
		for (Spot spot : spots) {
			if (spot.spotOwner != SpotOwner.X) {
				allSpotsAreX = false;
				break;
			}
		}

		boolean allSpotsAreO = true;
		for (Spot spot : spots) {
			if (spot.spotOwner != SpotOwner.O) {
				allSpotsAreO = false;
				break;
			}
		}

		return allSpotsAreX || allSpotsAreO;
	}
}