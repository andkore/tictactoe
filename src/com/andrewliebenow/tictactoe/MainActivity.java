package com.andrewliebenow.tictactoe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final Button newGame = (Button) findViewById(R.id.button1);

		newGame.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent gameActivity = new Intent(getBaseContext(),
						GameActivity.class);
				startActivity(gameActivity);
			}
		});
	}
}
