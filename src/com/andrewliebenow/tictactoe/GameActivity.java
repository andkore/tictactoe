package com.andrewliebenow.tictactoe;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.andrewliebenow.tictactoe.GameLogic.GameState;
import com.andrewliebenow.tictactoe.GameLogic.Outcome;
import com.andrewliebenow.tictactoe.GameLogic.Player;
import com.andrewliebenow.tictactoe.GameLogic.Spot;
import com.andrewliebenow.tictactoe.GameLogic.SpotLocation;
import com.andrewliebenow.tictactoe.GameLogic.SpotOwner;

public class GameActivity extends Activity {

	private TextView topLeft;
	private TextView topCenter;
	private TextView topRight;
	private TextView middleLeft;
	private TextView middleCenter;
	private TextView middleRight;
	private TextView bottomLeft;
	private TextView bottomCenter;
	private TextView bottomRight;
	private TextView gameStatus;

	private GameState gameState;
	private Map<SpotLocation, TextView> gameBoardInterfaceMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		this.topLeft = (TextView) findViewById(R.id.textView1);
		this.topCenter = (TextView) findViewById(R.id.textView2);
		this.topRight = (TextView) findViewById(R.id.textView3);
		this.middleLeft = (TextView) findViewById(R.id.textView4);
		this.middleCenter = (TextView) findViewById(R.id.textView5);
		this.middleRight = (TextView) findViewById(R.id.textView6);
		this.bottomLeft = (TextView) findViewById(R.id.textView7);
		this.bottomCenter = (TextView) findViewById(R.id.textView8);
		this.bottomRight = (TextView) findViewById(R.id.textView9);
		this.gameStatus = (TextView) findViewById(R.id.textView10);

		this.gameBoardInterfaceMap = new HashMap<SpotLocation, TextView>();

		this.gameBoardInterfaceMap.put(new SpotLocation(0, 0), this.topLeft);
		this.gameBoardInterfaceMap.put(new SpotLocation(0, 1), this.topCenter);
		this.gameBoardInterfaceMap.put(new SpotLocation(0, 2), this.topRight);
		this.gameBoardInterfaceMap.put(new SpotLocation(1, 0), this.middleLeft);
		this.gameBoardInterfaceMap.put(new SpotLocation(1, 1),
				this.middleCenter);
		this.gameBoardInterfaceMap
				.put(new SpotLocation(1, 2), this.middleRight);
		this.gameBoardInterfaceMap.put(new SpotLocation(2, 0), this.bottomLeft);
		this.gameBoardInterfaceMap.put(new SpotLocation(2, 1),
				this.bottomCenter);
		this.gameBoardInterfaceMap
				.put(new SpotLocation(2, 2), this.bottomRight);

		for (final Entry<SpotLocation, TextView> entry : gameBoardInterfaceMap
				.entrySet()) {
			entry.getValue().setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					tryToTakeSpot(entry.getKey());
				}
			});
		}

		this.gameState = new GameState();
		renderGameState();
	}

	void renderGameState() {
		renderGameStatus();
		renderGameBoard();
	}

	void renderGameStatus() {
		String gameStatus = null;
		if (this.gameState.outcome.equals(Outcome.IN_PROGRESS)) {
			String currentPlayer = null;
			if (this.gameState.currentPlayer.equals(Player.X)) {
				currentPlayer = "X";
			} else if (this.gameState.currentPlayer.equals(Player.O)) {
				currentPlayer = "O";
			}
			gameStatus = "It is " + currentPlayer + "'s turn";
		} else if (this.gameState.outcome.equals(Outcome.DRAW)) {
			gameStatus = "The game was a draw";
		} else {
			String playerWhoWon = null;
			if (this.gameState.outcome.equals(Outcome.X_WINS)) {
				playerWhoWon = "X";
			} else if (this.gameState.outcome.equals(Outcome.O_WINS)) {
				playerWhoWon = "O";
			}
			gameStatus = playerWhoWon + " won the game";
		}
		this.gameStatus.setText(gameStatus);
	}

	void renderGameBoard() {
		for (final Entry<SpotLocation, TextView> entry : this.gameBoardInterfaceMap
				.entrySet()) {
			TextView textView = entry.getValue();
			SpotLocation spotLocation = entry.getKey();
			Spot spot = this.gameState.gameBoard.getSpot(spotLocation);
			String spotText = spotText(spot.spotOwner);
			textView.setText(spotText);
		}
	}

	static String spotText(SpotOwner spotOwner) {
		String spotText = null;
		if (spotOwner.equals(SpotOwner.FREE)) {
			spotText = "_";
		} else if (spotOwner.equals(SpotOwner.X)) {
			spotText = "X";
		} else if (spotOwner.equals(SpotOwner.O)) {
			spotText = "O";
		}

		return spotText;
	}

	void tryToTakeSpot(SpotLocation spotLocation) {
		this.gameState.processPlay(spotLocation);
		renderGameState();
	}
}
